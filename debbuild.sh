#!/bin/sh

export DEBEMAIL="contact@j4s8.de"
export DEBFULLNAME="Joerg Steinkamp"
siril_tag="1.2.6"

git clone --depth 1 -b ${siril_tag}  https://gitlab.com/free-astro/siril.git

cd siril
git submodule sync --recursive
git submodule update --init --recursive

ln -s /usr/share/intltool/Makefile.in.in po/Makefile.in.in

dh_make --createorig -y -s -c gpl3 -p siril_${siril_tag}
dh_auto_configure --buildsystem=meson

dpkg-buildpackage -rfakeroot -us -uc -b
