To profit from the latest development in Siril under the current stable Debian release without compiling it locally, I created this little CI/CD script, which builds an deb-Package of the latest Siril release for the current stable Debian release (bullseye) and stores it as artifact under CI/CD.

Origin: https://gitlab.com/free-astro/siril
Docu: https://free-astro.org/index.php/Siril

